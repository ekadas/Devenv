#!/bin/bash

if [ $# -eq 0 ]; then
   basedir="$(dirname "$0")/.."
else
   basedir="$1"
fi
# shellcheck source=util.sh
source "$basedir/util.sh"

os=$(uname -s)
# this script installs or updates lsp servers and their dependencies

pprint() {
   print_green "\n→ $1"
}

npm_install() {
   pprint "$1"
   npm install -g "$1"
}

pprint efm-langserver
if [ "$os" = "Darwin" ]; then
   brew install efm-langserver
else
   sudo pacman -S efm-langserver
fi

pprint lua-language-server
if [ "$os" = "Darwin" ]; then
   brew install saadparwaiz1/personal/lua-language-server
else
   yaourt -S lua-language-server
   if [ -f "/bin/lua-language-server" ]; then
      sudo mv /bin/lua-language-server /bin/lua-langserver
   fi
fi

pprint "shell script"
npm install -g bash-language-server
if [ "$os" = "Darwin" ]; then
   brew install shfmt shellcheck
else
   sudo pacman -S shfmt shellcheck
fi

pprint java
if [ "$os" = "Darwin" ]; then
   jdtls_path=/usr/local/share/eclipse.jdt.ls

   if [ -d "$jdtls_path" ]; then
      (
         cd $jdtls_path || return
         git pp

         ./mvnw package -DSkipTests
      )
   else
      git clone git@github.com:eclipse/eclipse.jdt.ls.git $jdtls_path
      (
         cd $jdtls_path || return
         ./mvnw clean verify
      )
   fi

   cp "$basedir/tools/java/jdtls" /usr/local/bin/jdtls
fi
npm install -g prettier-plugin-java

pprint rust
if [ "$os" = "Darwin" ]; then
   brew install rust-analyzer
else
   sudo pacman -S rust-analyzer
fi

npm_install prettier

pprint "node & typescript"
npm install -g standard
npm install -g typescript
npm install -g typescript-language-server

pprint "json, html & css"
npm install -g vscode-langservers-extracted

npm_install yarn

pprint yaml
pip install --user yamllint
yarn global add yaml-language-server --prefix /usr/local

pprint elm
npm install -g elm
npm install -g elm-test
npm install -g elm-format
npm install -g @elm-tooling/elm-language-server

pprint docker
npm install -g dockerfilelint
if [ "$os" = "Darwin" ]; then
   brew install hadolint
else
   yaourt -S hadolint
fi

pprint yaml
npm install -g markdownlint
npm install -g markdownlint-cli
cp "$basedir/tools/nvim/markdownlintrc" ~/.markdownlintrc

pprint cfn-lint
pip install --user cfn-lint

pprint kubeval
if [ "$os" = "Darwin" ]; then
   brew install instrumenta/instrumenta/kubeval
else
   yaourt -S kubeval
fi
