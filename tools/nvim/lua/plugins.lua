-- lsp
require('lsp')

-- treesitter
require('treesitter')

-- colorize
require'colorizer'.setup()

-- commenter
require('Comment').setup()

-- whitespace
vim.opt.list = true
vim.opt.listchars:append("space:⋅")
require("indent_blankline").setup()

-- completion
require('completion')
